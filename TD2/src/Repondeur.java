import java.util.Stack;


public class Repondeur {
	private Message messageAcceuil;
	private Stack<Message> enregistrer;
	
	public Repondeur(String acceuil) {
		this.messageAcceuil = new Message(acceuil);
		this.enregistrer = new Stack<Message>();
	}
	
	public Repondeur() {
		this.messageAcceuil = new Message("Vous êtes sur le messagerie de Hugo Haquette, veuillez laisser un message après le signal sonore");
		this.enregistrer = new Stack<Message>();
	}
	
	public void diffuserMessageAcceuil() {
		System.out.println(this.messageAcceuil.get());
	}
	
	public void ecouterMessageRepondeur() {
		System.out.println(enregistrer.pop().get());
	}
	
	public void ecouterToutLesMessageRepondeur() {
		while(!enregistrer.isEmpty()) {
			System.out.println(enregistrer.pop().get());
		}
		
	}
	
	public void enregistrerMessageRepondeur(String msg) {
		enregistrer.push(new Message(msg));
	}
	
	
}
