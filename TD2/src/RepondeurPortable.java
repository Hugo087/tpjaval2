
public class RepondeurPortable extends Repondeur{
	private String numero;
	
	public RepondeurPortable(String numero) {
		super("Vous êtes sur la messagerie vocale du " + numero + ", votre correspondant est indisponible pour l'instant mais vous pouvez lui laisser un message après le bip sonore");
		this.setNumero(numero);
	}
	
	public RepondeurPortable(String numero, String msg) {
		super(msg);
		this.setNumero(numero);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
}
