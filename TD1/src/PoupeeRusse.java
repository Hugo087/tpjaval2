
public class PoupeeRusse {
	private PoupeeRusse poupeeContien;
	private PoupeeRusse poupeeContenu;
	private Boolean ouverte;
	private int taille;
	
	public PoupeeRusse(int taille) {
		this.taille = taille;
		this.poupeeContenu = null;
		this.poupeeContien = null;
		this.ouverte = false;
	}
	
	public PoupeeRusse(int taille, Boolean ouverte) {
		this.taille = taille;
		this.poupeeContenu = null;
		this.poupeeContien = null;
		this.ouverte = ouverte;
	}
	
	public void ouvrir() {
		if(this.ouverte == false) {
			this.ouverte = true;
		}
	}
	
	public void fermer() {
		if(this.ouverte == true) {
			this.ouverte = false;
		}
	}
	
	public void placerDans(PoupeeRusse p) {
		if(p != null && p.ouverte && !this.ouverte && p.taille > this.taille && this.poupeeContien == null && p.poupeeContenu == null) {
			this.poupeeContien = p;
			p.poupeeContenu = this;
		}
	}
	
	public void sortirDe() {
		if(this.poupeeContenu != null && this.poupeeContenu.ouverte) {
			this.poupeeContenu.poupeeContien = null;
			this.poupeeContenu = null;		
		}
	}
	
	public int nbPoupeesContenues() {
		if(this.poupeeContenu != null) {
			return this.poupeeContenu.nbPoupeesContenues() + 1;
		}
		else {
			return 1;
		}
	}
}
