import java.util.ArrayList;

public class ListeDePoupees {
	private ArrayList<PoupeeRusse> list;
	
	public ListeDePoupees() {
		this.list = new ArrayList<PoupeeRusse>();
	}
	
	public ListeDePoupees(ArrayList<PoupeeRusse> list) {
		this.list = list;
	}
	
	public void add(PoupeeRusse poupee) {
		if(!list.contains(poupee)) {
			this.list.add(poupee);
		}
	}
	
	public int length() {
		return list.size();
	}
	
	public PoupeeRusse get(int i) {
		return list.get(i);
	}
}
