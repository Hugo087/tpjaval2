/**  Classe ErrModif pour le TP3.
  * @author Hugo Haquette
*/

public class ErrModif extends Exception {
	
	// Attributs
	Integer val;
	
	// Constructeurs
	public ErrModif() {
		val = null;	
	}
	
	public ErrModif(int val) {
		this.val = val;
	}
	
	// Méthodes
	public int getValError() {
		return val;
	}
	
}
