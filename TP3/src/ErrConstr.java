/**  Classe ErrConstr pour le TP3.
  * @author Hugo Haquette
*/

public class ErrConstr extends Exception {
	
	// Attributs
	Integer val;
	
	// Constructeurs
	public ErrConstr() {
		val = null;
	}
	
	public ErrConstr(int val) {
		this.val = val;
	}
	
	// Méthodes
	public int getValError() {
		return val;
	}
	
}
