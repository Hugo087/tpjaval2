/**  Classe Palindrome pour le TP3.
  * @author Hugo Haquette
*/
//Marko.MLADENOVIC@UPHF.fr
import java.lang.StringBuilder;
import java.text.Normalizer;

public class Palindrome {
	
	// Méthodes statiques
	public static boolean palindrome1(String chaine) {
		int i = 0;
		int i2 = chaine.length() - 1;
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
	
	public static boolean palindrome2(String chaine) {
		StringBuilder reverse = new StringBuilder(chaine).reverse();	// Inverse la chaine de caracatère
		String chaineReverse = reverse.toString();
		if(chaine.equals(chaineReverse)){
			return true;
		}
		return false;
	}
	
	public static boolean palindrome3(String chaine) {
		chaine = chaine.replaceAll("[^a-zA-Z0-9]", "");		// Toutes les valeurs qui ne sont pas de a à z et de 0 à 9 sont remplacé par le chaine vide
		int i = 0;
		int i2 = chaine.length() - 1;	
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
	
	public static boolean palindrome4(String chaine) {
		chaine = chaine.replaceAll("[^a-zA-Z0-9]", "");
		int i = 0;
		int i2 = chaine.length() - 1;	
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
	
	public static boolean palindrome4(String chaine, boolean caseSensitive) {
		chaine = chaine.replaceAll("[^a-zA-Z0-9]", "");
		if(caseSensitive) {
			chaine = chaine.toLowerCase();			// Met en minuscule si le paramètre est saisie
		}
		int i = 0;
		int i2 = chaine.length() - 1;	
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
	
	public static boolean palindrome5(String chaine) {
		chaine = Normalizer.normalize(chaine, Normalizer.Form.NFD);		// Sépare les accents des lettres (ex: à devient a`)
		chaine = chaine.replaceAll("[^a-zA-Z0-9]", "");
		int i = 0;
		int i2 = chaine.length() - 1;	
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
	
	public static boolean palindrome5(String chaine, boolean caseSensitive) {
		if(caseSensitive) {
			chaine = chaine.toLowerCase();			
		}
		chaine = Normalizer.normalize(chaine, Normalizer.Form.NFD);
		chaine = chaine.replaceAll("[^a-zA-Z0-9]", "");
		int i = 0;
		int i2 = chaine.length() - 1;	
		while(i < i2) {
			if(chaine.charAt(i) != chaine.charAt(i2)) {
				return false;
			}
			i++;
			i2--;
		}
		return true;
	}
}
