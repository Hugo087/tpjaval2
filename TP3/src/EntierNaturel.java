/**  Classe EntierNaturel pour le TP3.
  * @author Hugo Haquette
*/

import java.util.Random;
import java.util.Scanner;

public class EntierNaturel {
	
	// Attributs
	private int entier;
	
	// Constructeurs
	public EntierNaturel() {	
		this.entier = 0;
	}
	
	public EntierNaturel(int entier) throws ErrConstr {
		if(entier < 0)
			throw new ErrConstr(entier);	// Lève une exception si l'entier saisi est inférieur à 0
		this.entier = entier;
	}
	
	// Méthodes
	public int getEntier() {
		Random a = new Random();
		a.nextInt();
		return this.entier;
	}
	
	public void setEntier(int entier) throws ErrConstr{
		if(entier < 0)
			throw new ErrConstr(entier);
		this.entier = entier;
	}
	
	public void incrementer() {
		this.entier++;
	}
	
	public void decrementer() throws ErrModif{
		if(entier == 0)
			throw new ErrModif(entier);
	}
	
	public String toString() {
		return String.valueOf(this.entier);
	}
	
	public static EntierNaturel[] SaisieEntiers() {
		Scanner scan = new Scanner(System.in);
		int nbval = -1;
		do {
			try {
				System.out.println("Nombre d'entier naturel à saisir : ");	
				nbval = scan.nextInt();
				new EntierNaturel(nbval);
			}
			catch(Exception e) {
				scan.nextLine();
				System.out.println("Il faut saisir un entier.");
			}
		}while(nbval <= 1);		// Tant que la valeur saisie n'est pas supérieure ou égale à 1, on redemande la saisie d'un entier
		EntierNaturel tab[] = new EntierNaturel[nbval];
		for(int i = 0; i < nbval; i++) {
			try {
				System.out.println("Entier naturel numéro " + i + " : ");
				tab[i] = new EntierNaturel(scan.nextInt());		// Demande un entier
			}
			catch(ErrConstr e) {	// Gestion des exceptions
				System.out.println(e.getValError() + " n'est pas un entier naturel.");
				i--;
			}
			catch(Exception e) {
				System.out.println("Il faut saisir un entier naturel.");
				scan.nextLine();
				i--;
			}

		}
		scan.close();
		return tab;
	}
	
	public static void AfficherEntiers(EntierNaturel[] entiers) {
		System.out.println("Les entiers naturels saisis sont :");
		for(int i = 0; i < entiers.length; i++) {
			if(i == 0)
				System.out.print(entiers[i]);
			else
				System.out.print(", " + entiers[i]);
		}
	}
	
}
