/**  Classe Point pour le TP1.
  * @author Hugo Haquette
*/

public class Point {
	
	// Attributs
	private double x;
	private double y;
	
	// Constructeurs
	public Point() {
		this.x = 0;
		this.y = 0;
	}
	
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	// Méthodes
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public void deplacerPoint(double x, double y) { // Déplace le points en fonction des paramètres
		this.x = x;
		this.y = y;
	}
	
	public void afficherPoint() {	// Affiche la position du point
		System.out.println("Position en x : " + x);
		System.out.println("Position en y : " + y);
	}
}
