/**  Classe Robot pour le TP1.
  * @author Hugo Haquette
*/

public class Robot {
	
	// Attributes
	private String nom;
	private int x;
	private int y;
	private Orientation orientation;
	
	// Constructeurs
	public Robot() {
		this.nom = "";
		this.x = 0;
		this.y = 0;
		this.orientation = Orientation.Nord;
	}
	
	public Robot(String nom, int x, int y, Orientation orientation) {
		this.nom = nom;
		this.x = x;
		this.y =y;
		this.orientation = orientation;
	}
	
	public Robot(Robot robot, String nom) {
		this.nom = nom;
		this.x = robot.x;
		this.y = robot.y;
		this.orientation = robot.orientation;
	}
	
	// Méthodes
	public void avancer() {	// Fait avancer le robot d'une case en fonction de son orientation
		switch(orientation) {
		case Nord:
			y++;
			break;
		case Est:
			x++;
			break;
		case Sud:
			y--;
			break;
		case Ouest:
			x--;
			break;
		}
	}
	
	public void avancer(int distance) {	// Fait avancer le robot d'un nombre de case passer en paremètre en fonction de son orientaion
		switch(orientation) {
		case Nord:
			y+= distance;
			break;
		case Est:
			x+= distance;
			break;
		case Sud:
			y-= distance;
			break;
		case Ouest:
			x-= distance;
			break;
		}
	}
	
	public void tournerADroit() { // Fait tourner le robot à droite
		switch(orientation) {
		case Nord:
			this.orientation = Orientation.Est;
			break;
		case Est:
			this.orientation = Orientation.Sud;
			break;
		case Sud:
			this.orientation = Orientation.Ouest;
			break;
		case Ouest:
			this.orientation = Orientation.Nord;
			break;
		}
	}
	
	public void tournerAGauche() { // Fait tourner le robot à gauche
		switch(orientation) {
		case Nord:
			this.orientation = Orientation.Ouest;
			break;
		case Est:
			this.orientation = Orientation.Nord;
			break;
		case Sud:
			this.orientation = Orientation.Est;
			break;
		case Ouest:
			this.orientation = Orientation.Sud;
			break;
		}
	}
	
	public void afficherPosition() { // Affiche la position du robot
		System.out.println("Robot du nom de " + this.nom + " : ");
		System.out.println("Position en x : " + x);
		System.out.println("Position en y : " + y);
		System.out.println("Orientation du robot : " + this.orientation.getNomOrientation());
	}
}
