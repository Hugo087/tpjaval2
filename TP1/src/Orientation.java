/**  Enumération de l'orientation cardinal pour le TP1
  * @author Hugo Haquette
*/


public enum Orientation {
	Nord(0, "Nord"),
	Est(1, "Est"),
	Sud(2, "Sud"),
	Ouest(3, "Ouest"); // Déclaration de l'énumétaion
	
	// Attributs
	private int orientation;
	private String nomOrientation;
	
	// Constructeur
	Orientation(int orientation, String nomOrientation){
		this.orientation = orientation;
		this.nomOrientation = nomOrientation;
	}
	
	// Méthodes
	public int getOrientation() { // Retourne un numéro qui correspond à l'orientation
		return this.orientation;
	}
	
	public String getNomOrientation() {	// Retourne l'orientation 
		return this.nomOrientation;
	}
}
