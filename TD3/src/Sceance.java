import java.util.Date;

public class Sceance {

	private float prix;
	private float prixReduit;
	private Date date;
	private String titreFilm;
	
	public Sceance() {
		this.prix = 0;
		this.prixReduit = 0;
		this.date = new Date();
		this.titreFilm = "";
	}
	
	public Sceance(float prix, float prixReduit, Date date, String titreFilm) {
		this.prix = prix;
		this.prixReduit = prixReduit;
		this.date = date;
		this.titreFilm = titreFilm;
	}
	
	public float getPrix() {
		return prix;
	}
	
	public float getPrixReduit() {
		return prix * prixReduit;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getTitreFilm() {
		return titreFilm;
	}
}
