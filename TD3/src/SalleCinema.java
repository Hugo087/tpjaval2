import java.util.ArrayList;
import java.util.Date;

public class SalleCinema {

	private int places;
	private String lieu;
	private ArrayList<Sceance> sceances; 
	
	public SalleCinema() {
		this.places = 50;
		this.lieu = "";
		this.sceances = new ArrayList<Sceance>();
	}
	
	public SalleCinema(int places, String lieu) {
		this.places = places;
		this.lieu = lieu;
		this.sceances = new ArrayList<Sceance>();
	}
	
	public SalleCinema(int places, String lieu, Sceance sceances) {
		this.places = places;
		this.lieu = lieu;
		this.sceances = new ArrayList<Sceance>();
		this.sceances.add(sceances);
	}
	
	public SalleCinema(int places, String lieu, ArrayList<Sceance> sceances) {
		this.places = places;
		this.lieu = lieu;
		this.sceances = sceances;		
	}
	
	public void AjouterSceance(Sceance sceance) {
		this.sceances.add(sceance);
	}
	
	public void AjouterSceance(float prix, float prixReduit, Date date, String titreFilm) {
		this.sceances.add(new Sceance(prix, prixReduit, date, titreFilm));
	}
	
	public int getNombrePlace() {
		return places;
	}
	
	public String getLieu() {
		return lieu;
	}
	
	public ArrayList<Sceance> getListSceance(){
		return sceances;
	}
}
