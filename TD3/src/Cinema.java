import java.util.ArrayList;
import java.util.Date;

public class Cinema {

	private ArrayList<SalleCinema> salles;
	private ArrayList<Reservation> reservations;
	
	public Cinema() {
		this.salles = new ArrayList<SalleCinema>();
	}
	
	
	public Cinema(ArrayList<SalleCinema> salles) {
		this.salles = salles;
	}
	
	public void AjouterSalle(SalleCinema salle) {
		this.salles.add(salle);
	}
	
	public void AjouterSalle(int nbPlaces, String lieu ) {
		this.salles.add(new SalleCinema(nbPlaces, lieu));
	}
	
	public SalleCinema getSalle(int index) {
		return salles.get(index);
	}
	
	public void vendreBillet(Date date) {
	}
	
}
