import java.util.Calendar;
import java.util.Date;

public class Reservation {
	private SalleCinema salle;
	private Date date;
	private Boolean status;
	
	public Reservation(SalleCinema salle, Date date) {
		this.salle = salle;
		this.date = date;
		this.status = true;
	}
	
	public Reservation(SalleCinema salle) {
		this.salle = salle;
		this.date = Calendar.getInstance().getTime();
		this.status = true;
	}
	
	public void annulation() {
		this.status = false;
	}
}
