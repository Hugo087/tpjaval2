
public class Pion extends Piece{
	private Case[] casesPotentiels;
	private static final short valeur = 1;
	private String couleur;
	
	public Pion(Case[] casesPotentiels, String couleur, Case caseCourante, Case casePrecedante) {
		super(caseCourante,casePrecedante, true);
		this.casesPotentiels = casesPotentiels;
		this.couleur = couleur;	
	}
	
	public Pion(String couleur, Case caseCourante) {
		super(caseCourante,caseCourante, true);
		this.casesPotentiels = new Case[30];
		this.couleur = couleur;
	}
	
	public String toString() {
		return "p" + couleur ;
	}
	
}
