import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class Echiquier {
	private Case[] tableau;
	private ArrayList<Piece> pieces;
	
	public Echiquier() {
		this.tableau = new Case[64];
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				this.tableau[(8*i)+j] = new Case((char)('a'+ j) , 8-i);
			}		
		}
		this.pieces = new ArrayList<Piece>();
	}
	
	public void placer(Piece piece, Case caseCourante) {
		piece.getCaseCourante();
		pieces.add(piece);
	}
	
	public void positionDeDepart() {
		
		pieces.add(new Tour("Noir", this.tableau[0]));
		pieces.add(new Chevalier("Noir", this.tableau[1]));
		pieces.add(new Fou("Noir", this.tableau[2]));
		pieces.add(new Reine("Noir", this.tableau[3]));
		pieces.add(new Roi("Noir", this.tableau[4]));
		pieces.add(new Fou("Noir", this.tableau[5]));
		pieces.add(new Chevalier("Noir", this.tableau[6]));
		pieces.add(new Tour("Noir", this.tableau[7]));
		for(int i = 0; i < 8; i++) {
			this.pieces.add(new Pion("Noir", this.tableau[8+i]));
		}
		for(int i = 0; i < 8; i++) {
			this.pieces.add(new Pion("Blanc", this.tableau[(8*6) + i]));
		}	
		pieces.add(new Tour("Blanc", this.tableau[(8*7)]));
		pieces.add(new Chevalier("Blanc", this.tableau[(8*7)+1]));
		pieces.add(new Fou("Blanc", this.tableau[(8*7)+2]));
		pieces.add(new Reine("Blanc", this.tableau[(8*7)+3]));
		pieces.add(new Roi("Blanc", this.tableau[(8*7)+4]));
		pieces.add(new Fou("Blanc", this.tableau[(8*7)+5]));
		pieces.add(new Chevalier("Blanc", this.tableau[(8*7)+6]));
		pieces.add(new Tour("Blanc", this.tableau[(8*7)+7]));
	
	}
	
	public JFrame afficheTableau() {
		int k = 0;
		JFrame t = new JFrame();
		JPanel panel = new JPanel(new GridLayout (8,8));
		for(int i = 0; i<8;i++){
			for(int j = 0; j < 8; j++) {
				JPanel cas = new JPanel();
				  if(i%2 == 0 && j%2 == 0) {
				    cas.setBackground(Color.black);
				  }
				  else if(i%2 == 1 && j%2 == 1) {
					cas.setBackground(Color.black);
				  }
				   cas.setBorder(BorderFactory.createLineBorder(Color.black, 1));
				   panel.add(cas);
				}   
			}
			for(int i = 0; i < pieces.size(); i++) {
				int premier = pieces.get(i).getCaseCourante().premierToInt() - 1;
				int deuxieme = pieces.get(i).getCaseCourante().getDeuxieme() - 1;
				JLabel a = new JLabel(pieces.get(i).toString());
				JPanel p = ((JPanel)panel.getComponent(((7-deuxieme) * 8) + premier));
				if(p.getBackground() == Color.BLACK) {
					a.setForeground(Color.WHITE);
				}
				else {
					a.setForeground(Color.BLACK);
				}				
				p.add(a);
			}
			t.setSize(new Dimension(1000, 1000));
			t.add(panel);
			t.setVisible(true);
			return t;
	}
	
	public Case[] getCase() {
		return this.tableau;
	}
	
	public Piece getPiece(char premier, int deuxieme) {
		for(Piece p : pieces){
			if(p.getCaseCourante().getPremier() == premier && p.getCaseCourante().getDeuxieme() == deuxieme) {
				return p;
			}
		}
		return null;
	}
	
	public Case getCase(char premier, int deuxieme) {
		for(Case c : this.tableau) {
			if(c.getPremier() == premier && c.getDeuxieme() == deuxieme) {
				return c;
			}
		}
		return null;
	}
	
	public void mouvement(Piece p, Case c) {
		p.setCasePrecedente(p.getCaseCourante());
		p.setCaseCourante(c);
		System.out.println(p.getCaseCourante());
	}
}
