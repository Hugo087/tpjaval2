
public abstract class Piece {
	private Case caseCourante;
	private Case casePrecedente;
	private boolean actif;
	
	public Piece(Case caseCourante, Case casePrecedente, boolean actif) {
		this.caseCourante = caseCourante;
		this.casePrecedente = casePrecedente;
		this.actif = actif;
	}
	
	public boolean equals(Object other) {
		if(other instanceof Piece) {
			Piece otherPiece= (Piece) other;
			return otherPiece.caseCourante.equals(this.caseCourante) && otherPiece.casePrecedente.equals(this.casePrecedente) && otherPiece.actif == this.actif;
		}
		return false;
	}
	
	public String toString() {
		return this.getClass().getName() + ", case courante : " + caseCourante.toString() + "\nCase precedente : " + casePrecedente.toString() + "\nActif : " + actif + "\n";
	}
	
	public Case getCaseCourante() {
		return caseCourante;
	}
	
	public Case getCasePrecedente() {
		return casePrecedente;
	}
	
	public void setCaseCourante(Case caseCourante) {
		this.caseCourante = caseCourante;
	}
	
	public void setCasePrecedente(Case casePrecedente) {
		this.casePrecedente = casePrecedente;
	}

}
