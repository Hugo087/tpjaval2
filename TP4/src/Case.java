
public class Case {
	private Character premier;
	private Integer deuxieme;
	
	public Case(char premier, int deuxieme) {
		this.premier = premier;
		this.deuxieme = deuxieme;
	}
	
	public boolean equals(Object other) {
		if(other instanceof Case) {
			Case otherCase = (Case) other;
			return (( this.premier == otherCase.premier || ( this.premier != null && otherCase.premier != null && this.premier.equals(otherCase.premier))) && (this.deuxieme == otherCase.deuxieme || (this.deuxieme != null && otherCase.deuxieme != null && this .deuxieme.equals(otherCase.deuxieme))));
		}
		return false;
	}
	
	public String toString() {
		return "(" + premier + ", " + deuxieme + ")";
	}
	
	public char getPremier() {
		return premier;
	}
	
	public void setPremier(char premier) {
		this.premier = premier;
	}
	
	public int getDeuxieme() {
		return deuxieme;
	}
	
	public void setDeuxieme(int deuxieme) {
		this.deuxieme = deuxieme;
	}
	
	public int premierToInt() {
		switch(this.premier) {
			case 'a':
				return 1;
			case 'b':
				return 2;
			case 'c':
				return 3;
			case 'd':
				return 4;
			case 'e':
				return 5;
			case 'f':
				return 6;
			case 'g':
				return 7;
			case 'h':
				return 8;
			default :
				return 0;
		}
	}
}
