/**  Classe Employe pour le TP2.
  * @author Hugo Haquette
*/

public abstract class Employe {
	
	// Attributs
	private String nom;
	private float salaire;
	
	
	// Constructeurs
	public Employe(String nom,float salaire) {
		this.nom = nom;
		this.salaire = salaire;
	}
	
	public Employe(String nom) {
		this.nom = nom;
		this.salaire = 0;
	}
	
	public Employe() {
		this.nom = "";
		this.salaire = 0;
	}
	
	// Méthodes
	public float getSalaire() {  // Récupére le salaire d'un employé
		return this.salaire;
	}
	
	public String getNom() {   // Récupére le nom de l'employé
		return this.nom;
	}
}
