/**  Classe Developpeur pour le TP2.
  * @author Hugo Haquette
*/

import java.util.ArrayList;

public class Developpeur extends Employe{
	
	// Attributs
	private String nomEquipe;
	private ArrayList<String> listLangage;
	
	// Constructeurs
	public Developpeur(String nom, float salaire, String nomEquipe, String langage) {
		super(nom, salaire);
		this.nomEquipe = nomEquipe;
		this.listLangage = new ArrayList<String>();
		this.listLangage.add(langage);
	}
	
	public Developpeur(String nom, float salaire, String nomEquipe, ArrayList<String> langages) {
		super(nom, salaire);
		this.nomEquipe = nomEquipe;
		this.listLangage = langages;
	}
	
	public Developpeur(String nom) {
		super(nom);
		this.nomEquipe = "";
		this.listLangage = new ArrayList<String>();
	}
	
	public Developpeur() {
		super();
		this.nomEquipe = "";
		this.listLangage = new ArrayList<String>();
	}
	
	// Méthodes
	public void ajouterLangage(String langage) { // Ajoute le langage à la liste des langages connus du développeur
		this.listLangage.add(langage);
	}
	
	public String getNomEquipe() {	// Récupère le nom de l'équipe du développeur
		return this.nomEquipe;
	}
	
	public ArrayList<String> getListLangage(){	// Récupère tout les langages connus du développeur
		return listLangage;
	}
	
	public boolean connaisLangage(String langage) {	// Renvoie true si le développeur connais le langage mis ne paramètre
		return listLangage.contains(langage);
	}
	
}
