/**  Classe Entreprise pour le TP2.
  * @author Hugo Haquette
*/

import java.util.ArrayList;

public class Entreprise {
	
	// Attributs
	private ArrayList<Employe> employes;
	private String nomEntreprise;
	public static int hello;
	
	// Constructeurs
	public Entreprise() {
		this.nomEntreprise = "";
		this.employes = new ArrayList<Employe>();
	}
	
	public Entreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
		this.employes = new ArrayList<Employe>();
	}
	
	public Entreprise(String nomEntreprise, ArrayList<Employe> employe) {
		this.nomEntreprise = nomEntreprise;
		this.employes = employe;
	}
	
	// Méthodes
	public String getNomEntreprise() {	// Récupère le nom de l'entreprise
		return nomEntreprise;
	}
	
	public ArrayList<Employe> getListOfEmploye(){	// Renvoie la liste des employés
		return employes;
	}
	
	public void ajouterEmployer(Employe emp) {	// Ajoute un employé à l'entreprise
		this.employes.add(emp);
	}
	
	public void afficherChefProjet(float budget) {	// Affiche les chefs de projets ayant un budget supérieur au budget mis en paramètre
		for(Employe emp : employes) {
			if(emp instanceof ChefProjets && ((ChefProjets) emp).getBudget() > budget) {
				System.out.println(emp.getNom());				
			}
		}
	}
	
	public void afficherDeveloppeur(String langage) {	// Affiche les développeur qui connaissent le langage mis en paramètre
		for(Employe emp : employes) {
			if(emp instanceof Developpeur && ((Developpeur) emp).connaisLangage(langage)) {
				System.out.println(emp.getNom());
			}
		}
	}
}
