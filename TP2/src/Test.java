/**  Classe de test pour le TP2.
  * @author Hugo Haquette
*/


public class Test {

	public static void main(String[] args) {
		
		var chef1 = new ChefProjets("Maxime", 3000, "TeamRed", 50000);
		var chef2 = new ChefProjets("Fabien", 4500, "TeamBlue", 55000);
		var dev1 = new Developpeur("Zachary", 1900, "TeamRed", "C#");
		var dev2 = new Developpeur("Hugo", 1800, "TeamRed", "Java");
		dev2.ajouterLangage("C#");
		var dev3 = new Developpeur("Damien", 2500, "TeamBlue", "Java");
		var entreprise = new Entreprise("Kenora");
		entreprise.ajouterEmployer(chef1);
		entreprise.ajouterEmployer(chef2);
		entreprise.ajouterEmployer(dev1);
		entreprise.ajouterEmployer(dev2);
		entreprise.ajouterEmployer(dev3);
		System.out.println("Entreprise " + entreprise.getNomEntreprise() + " : ");
		System.out.println("Chefs de projets ayant un budget supérieur à 51000 : ");
		entreprise.afficherChefProjet(51000);
		System.out.println("Programmeurs connaissant le C# : ");
		entreprise.afficherDeveloppeur("C#");
		Entreprise.hello = 4;
		entreprise.hello = 3;
		
	}

}
