/**  Classe ChefProjets pour le TP2.
  * @author Hugo Haquette
*/

public class ChefProjets extends Employe {
	
	// Attributs
	private String nomEquipe;
	private float budget;
	
	// Constructeurs
	public ChefProjets(String nom, float salaire, String nomEquipe, float budget) {
		super(nom, salaire);
		this.nomEquipe = nomEquipe;
		this.budget = budget;
	}
	
	public ChefProjets(String nom, String nomEquipe) {
		super(nom);
		this.nomEquipe = nomEquipe;
		this.budget = 0;
	}
	
	public ChefProjets(String nom) {
		super(nom);
		this.nomEquipe = "";
		this.budget = 0;
	}
	
	public ChefProjets() {
		super();
		this.nomEquipe = "";
		this.budget = 0;
	}
	
	// Méthodes
	public String getNomEquipe() {	// Récupère le nom de l'équipe
		return nomEquipe;
	}
	
	public float getBudget() {	// Récupère le budget du chef de projet
		return budget;
	}
	
}
