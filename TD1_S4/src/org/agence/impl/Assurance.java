package org.agence.impl;

public class Assurance extends Compte {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3401424937019718334L;
	private float interet;
	
	public Assurance(Client client, float argent, float interet) {
		super(client, argent);
		this.interet = interet;
	}

	@Override
	public boolean retrait(float money) { 
		return false;
	}

	public float getInteret() {
		return interet;
	}

	protected void setInteret(float interet) {
		this.interet = interet;
	}
	
	public void faireInteret() {
		versement(getArgent() * interet);
	}

	@Override
	public String toString() {
		return "Compte assurance " + super.getNum() + " de " + this.getClient().getPrenom() + " " + this.getClient().getNom() + " avec " + this.getArgent() + " euros et " + "avec un taux d'iteret de " + this.interet + " %";
	}
	
}
