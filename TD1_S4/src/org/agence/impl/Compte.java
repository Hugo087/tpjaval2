package org.agence.impl;

import java.io.Serializable;

public abstract class Compte implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5760416351071558923L;
	private static int nextNum = 1;
	private int num;
	private Client client;
	private float argent;
	
	public Compte(Client client, float argent){
		this.num = nextNum;
		nextNum++;
		this.client = client;
		this.argent = argent;
	}
	
	public abstract String toString();
	
	public void versement(float money) {
		argent += money;
	}
	
	public boolean retrait(float money) {
		if(argent - money > 0) {
			argent -= money;
			return true;
		}
		return false;
	}
	
	public void virement(Compte compte, float money) {
		if(money > 0) {
			if(retrait(money)) {
				compte.versement(money);
			}
		}
	}
	
	public float getArgent() {
		return argent;
	}
	
	public int getNum() {
		return num;
	}
	
	public Client getClient() {
		return client;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}
	
	public boolean estADecouvert() {
		if(this.argent < 0) {
			return true;
		}
		return false;
	}
	
}

