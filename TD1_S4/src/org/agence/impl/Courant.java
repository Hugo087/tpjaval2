package org.agence.impl;

public class Courant extends Compte{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3084149465822971257L;
	private float decouvert;
	private Carte carte;
	
	public Courant(Client client, float argent, float decouvert, Carte carte) {
		super(client, argent);
		this.setDecouvert(decouvert);
		this.setCarte(carte);
	}
	
	public Courant(Client client, float argent, float decouvert) {
		super(client, argent);
		this.setDecouvert(decouvert);
		carte = null;
	}

	@Override
	public boolean retrait(float money){
		if(money > 0) {
			if(getArgent() + getDecouvert() > 0) {
				super.retrait(money);
				return true;
			}
		}
		return false;
	}

	public float getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(float decouvert) {
		this.decouvert = decouvert;
	}

	public Carte getCarte() {
		return carte;
	}

	public void setCarte(Carte carte) {
		this.carte = carte;
	}
	
	public void retirerCarte() {
		this.carte = null;
	}

	@Override
	public String toString() {
		return "Compte courant " + super.getNum() + " de " + this.getClient().getPrenom() + " " + this.getClient().getNom() + " avec " + this.getArgent() + " euros et " + this.decouvert + " euros de decouverts possible"; 
	}
	
}
