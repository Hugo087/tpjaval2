package org.agence.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Client implements org.agence.itf.Client, Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1397434254282281705L;

	private static int nextId = 1;
	
	private int id;
	private String nom, prenom;
	private List<Compte> comptes;
	
	public Client(String nom, String prenom, List<Compte> comptes) {
		this.id = nextId;
		nextId++;
		this.nom = nom;
		this.prenom = prenom;
		this.comptes = comptes;
	}
	
	public Client(String nom, String prenom) {
		this.id = nextId;
		nextId++;
		this.nom = nom;
		this.prenom = prenom;
		comptes = new ArrayList<Compte>();
	}
	
	public Client( String nom, String prenom, Compte compte) {
		this.id = nextId;
		nextId++;
		this.nom = nom;
		this.prenom = prenom;
		this.comptes = new ArrayList<Compte>();
		this.comptes.add(compte);
	}
	
	public void ajoutCpt(Compte compte) {
		comptes.add(compte);
	}
	
	public void supprCpt(int num) {
		comptes.removeIf(n -> (n.getNum() == num));
	}
	
	public int getId() {
		return this.id;
	}
	
	public float getPatrimoine() {
		float total = 0;
		for(Compte compte : comptes) {
			total += compte.getArgent();
		}
		return total;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public String getPrenom() {
		return this.prenom;
	}
	
	public List<Compte> getComptes(){
		return this.comptes;
	}
	
	public String toString() {
		return "Client numero " + this.getId() + " " + this.getPrenom() + " " + this.getNom() + " qui a " + this.comptes.size() + " comptes";
	}

}
