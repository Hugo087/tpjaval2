package org.agence.impl;

import java.io.Serializable;

public class Carte implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7406484832487096989L;

	private static int nextNum = 0;
	
	private int numCarte;
	private String nom;
	private String prenom;
	
	public Carte(String nom, String prenom) {
		this.numCarte = nextNum;
		nextNum++;
		this.nom = nom;
		this.prenom = prenom;
	}

	public int getNumCarte() {
		return numCarte;
	}
}
