package org.agence.impl;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.io.Serializable;

import org.agence.itf.Banque;

public class Agence implements Banque, Serializable{
	
	private static final long serialVersionUID = -5317267895833735024L;
	
	private List<Client> clients;
	private String nom;
	private List<Compte> comptes;
	
	public Agence(String nom) {
		this.clients = new ArrayList<Client>();
		this.comptes = new ArrayList<Compte>();
		this.nom = nom;
	}
	
	public Agence(String nom, List<Client> clients, List<Compte> comptes) {
		this.clients = clients;
		this.comptes = comptes;
		this.nom = nom;
	}
	
	public void ajouterClient(Client client) {
		for(Compte compte : client.getComptes()) {
			compte.setClient(client);
		}
		this.clients.add(client);
	}
	
	public void retirerClient(int id) {
		this.clients.removeIf(n -> (n.getId() == id));
	}
	
	public void ajouterCompte(Compte compte) {
		if(compte.getClient() != null)
			compte.getClient().ajoutCpt(compte);
		this.comptes.add(compte);
	}
	
	public void retirerCompte(int num) {
		this.comptes.removeIf(n -> (n.getNum() == num));
	}
	
	public void creeClientPar(String nom, String prenom) {
		clients.add(new ClientPar(nom, prenom));
	}
	
	public void creeClientPro(String nom, String prenom) {
		clients.add(new ClientPro(nom, prenom));
	}
	
	public Client getClient(int id) {
		for(Client client : this.clients) {
			if(client.getId() == id) {
				return client;
			}
		}
		return null;
	}
	
	public Client getClient(String prenom, String nom) {
		for(Client temp : this.clients) {
			if(temp.getPrenom().equals(prenom) && temp.getNom().equals(nom)) {
				return temp;
			}
		}
		return null;
	}
	
	public Compte getCompte(int num) {
		for(Compte compte : this.comptes) {
			if(compte.getNum() == num) {
				return compte;
			}
		}
		return null;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void ajouterCarte(Client c) {
		for(Compte cpt : c.getComptes()) {
			if(cpt instanceof Courant) {
				((Courant) cpt).setCarte(new Carte(c.getNom(), c.getPrenom()));
			}
		}
	}
	
	public List<Compte> getComptes() {
		return this.comptes;
	}
	
	public List<Client> getClients() {
		return this.clients;
	}
	
	public void changerInteretAssurance(float interet) {
		for(Compte compte : comptes) {
			if(compte instanceof Assurance) {
				((Assurance) compte).setInteret(interet);
			}
		}
	}
	
	public List<Compte> comptesADecouvert(){
		List<Compte> res = new ArrayList<Compte>();
		for(Compte compte : comptes) {
			if(compte.estADecouvert()) {
				res.add(compte);
			}
		}
		return res;
	}
	
	public void retirerCarteSiDecouvert() {
		List<Compte> decouvert = comptesADecouvert();
		for(Compte compte : decouvert) {
			if(compte instanceof Courant) {
				((Courant) compte).retirerCarte();
			}
		}
	}
	
	public Client clientLePlusRiche() {
		float max = Float.NEGATIVE_INFINITY, tot = 0; Client m = null;
		for (Client c : this.clients)
		{
			for (Compte compte : c.getComptes())
			{
				tot += compte.getArgent();
			}
			if (tot>max)
			{
				max = tot;
				m = c;
			}
			tot = 0;
		}	
		return m;	
	}
	
	public String toString() {
		String res = "Agence " + this.nom + " avec comme client : \n";
		for(Client client : clients) {
			res += client.toString() + "\n";
		}
		return res;
	}
	
	public void saveData(String chemin) {
		File f = new File(chemin);
		try {
			FileOutputStream outFile = new FileOutputStream(f);
			ObjectOutputStream outObject = new ObjectOutputStream(outFile);
			outObject.writeObject(this);
			outObject.close();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public static Agence loadData(String chemin) {
		File f = new File(chemin);
		Agence agence = null;
		try {
			FileInputStream inFile = new FileInputStream(f);
			ObjectInputStream inObject = new ObjectInputStream(inFile);
			agence = (Agence)inObject.readObject();
			inObject.close();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		return agence;
	}
	
	public void saveDataTxt(String chemin) {
		String data;
		File f = new File(chemin);
		data = this.toString();
		for(Compte a : this.comptes) {
			data += a.toString() + "\n";
		}
		try {
			
			FileOutputStream outfile = new FileOutputStream(f);
			PrintWriter outPrint = new PrintWriter(outfile);
			outPrint.print(data);
			outPrint.close();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static Agence loadDataTxt(String chemin) {
		String data = "", buf;
		File f = new File(chemin);
		try {
			FileReader infile = new FileReader(f);
			BufferedReader buffer = new BufferedReader(infile);
			while((buf = buffer.readLine()) != null) {
				data += buf + "\n";
			}
			buffer.close();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		String temp[] = data.split("\n");
		Agence res = null;
		try {
		for(String io : temp) {
			switch(io.split(" ")[0]) {
				case "Agence" :
					res = new Agence(io.split(" ")[1]);
					break;
				case "Client" :
					if(io.split(" ")[1].equals("particulier")) {
						res.ajouterClient(new ClientPar(io.split(" ")[5], io.split(" ")[4]));
					}
					else {
						res.ajouterClient(new ClientPro(io.split(" ")[5], io.split(" ")[4]));
					}
					break;
				case "Compte" :
					switch(io.split(" ")[1]) {
					case "courant" :
						res.ajouterCompte(new Courant(res.getClient(io.split(" ")[4], io.split(" ")[5]), Float.parseFloat(io.split(" ")[7]), Float.parseFloat( io.split(" ")[10])));
						break;
					case "assurance" :
						res.ajouterCompte(new Assurance(res.getClient(io.split(" ")[4], io.split(" ")[5]), Float.parseFloat(io.split(" ")[7]), Float.parseFloat( io.split(" ")[15])));
						break;
					case "epargne" :
						res.ajouterCompte(new Epargne(res.getClient(io.split(" ")[4], io.split(" ")[5]), Float.parseFloat(io.split(" ")[7]), Float.parseFloat( io.split(" ")[14]), Float.parseFloat( io.split(" ")[20])));
						break;
					}
					break;
			}
		}
		}
		catch(Exception e) {
			return null;
		}
		
		
		return res;
	}
	
}
