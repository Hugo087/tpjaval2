package org.agence.impl;

import java.util.List;

public class ClientPar extends Client{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4007798946662989050L;

	public ClientPar(String nom, String prenom, List<Compte> comptes) {
		super(nom, prenom, comptes);
	}

	public ClientPar(String nom, String prenom) {
		super(nom, prenom);
	}

	public ClientPar(String nom, String prenom, Compte compte) {
		super(nom, prenom, compte);
	}
	
	@Override
	public String toString() {
		return "Client particulier numero " + this.getId() + " " + this.getPrenom() + " " + this.getNom() + " qui a " + super.getComptes().size() + " comptes";
	}
}
