package org.agence.impl;

import java.util.ArrayList;
import java.util.List;

public class ClientPro extends Client{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5455667825023601314L;

	public ClientPro(String nom, String prenom, Compte compte) {
		super(nom, prenom, toCourant(compte));
	}

	public ClientPro(String nom, String prenom, List<Compte> comptes) {
		super(nom, prenom, toCourant(comptes));
	}

	public ClientPro(String nom, String prenom) {
		super(nom, prenom);
	}

	private static List<Compte> toCourant(List<Compte> comptes) {
		List<Compte> res = new ArrayList<Compte>();
		for(Compte compte : comptes) {
			if(compte instanceof Courant) {
				res.add(compte);
			}
		}
		return res;
	}
	
	private static Compte toCourant(Compte compte) {
		if(compte instanceof Courant) {
			return compte;
		}
		else {
			return null;
		}
	}
	
	@Override
	public void ajoutCpt(Compte compte) {
		if(compte instanceof Courant) {
			super.ajoutCpt(compte);
		}
	}
	
	@Override
	public String toString() {
		return "Client professionnel numero " + this.getId() + " " + this.getPrenom() + " " + this.getNom() + " qui a " + super.getComptes().size() + " comptes";
	}
}
