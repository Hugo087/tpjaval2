package org.agence.impl;


public class Epargne extends Compte{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6590947336260027310L;
	private float plafond;
	private float interet;
	
	public Epargne(Client client, float argent, float plafond, float interet) {
		super(client, argent);
		this.plafond = plafond;
		this.interet = interet;
	}
	
	@Override
	public void versement(float money) {
		if(getArgent() + money < plafond) {
			super.versement(money);
		}
	}
	
	public void faireInteret() {
		versement(getArgent() * interet);
	}

	@Override
	public String toString() {
		return "Compte epargne " + super.getNum() + " de " + this.getClient().getPrenom() + " " + this.getClient().getNom() + " avec " + this.getArgent() + " euros avec un taux d'iteret de " + this.interet + " % et un plafond de " + this.plafond + " euros";
	}
}
