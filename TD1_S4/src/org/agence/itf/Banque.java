package org.agence.itf;

import java.util.List;

import org.agence.impl.Client;

public interface Banque {
	
	public void ajouterClient(Client client);
	public void retirerClient(int id);
	public Client getClient(int id);
	public Client clientLePlusRiche();
	public List<Client> getClients();
}
