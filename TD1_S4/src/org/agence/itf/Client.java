package org.agence.itf;

public interface Client {
	
	public int getId();
	public String getPrenom();
	public String getNom();
}
