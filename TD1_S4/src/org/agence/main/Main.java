package org.agence.main;

import java.util.List;
import java.util.Scanner;

import org.agence.impl.Agence;
import org.agence.impl.Assurance;
import org.agence.impl.Client;
import org.agence.impl.ClientPar;
import org.agence.impl.ClientPro;
import org.agence.impl.Compte;
import org.agence.impl.Courant;
import org.agence.impl.Epargne;

public class Main {
	
	public static void main(String[] args) {
		Agence agence = new Agence("tes");
		agence.ajouterClient(new ClientPar("Haquette", "Hugo"));
		Client client = agence.getClient(1);
		agence.ajouterCompte(new Courant(client, 2000, 3000));
		agence.ajouterCompte(new Assurance(client, 2000, 3));
		agence.ajouterCompte(new Epargne(client, 2000, 1, 3));
		agence.ajouterCarte(client);
		client = new ClientPro("Tibi","Anthea");
		agence.ajouterClient(client);
		agence.ajouterCompte(new Courant(client, 3, 0));
		System.out.println(client.getComptes().size());
		agence.saveDataTxt("test.txt");
		Agence test = Agence.loadDataTxt("test.txt");
		System.out.println(test); 
	}
}
